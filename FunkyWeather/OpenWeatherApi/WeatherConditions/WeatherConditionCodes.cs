﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace OpenWeatherApi.WeatherConditions
{
    /// <summary>
    /// Stellt Funktionalität für das Mapping von Wetter-IDs auf Wetterbezeichnungen zur Verfügung
    /// </summary>
    public class WeatherConditionCodes
    {
        #region vars
        private Dictionary<int, string> myConditionMap;
        #endregion

        #region singleton stuff
        //instance
        //private static WeatherConditionCodes myInstance;

        //constructor
        public WeatherConditionCodes()
        {
            this.myConditionMap = GetConditionsAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// Stellt eine eindeutige Zuordnung von Wetter-IDs auf deutsche Wetterbezeichnungen zur Verfügung
        /// </summary>
        public Dictionary<Int32, String> Conditions
        {
            get
            {
                //if (myInstance == null)
                //{
                //    myInstance = new WeatherConditionCodes();
                //}
                //return myInstance.ConditionMap;
                return this.myConditionMap;
            }
        }
        #endregion
        
        #region props
        private Dictionary<Int32, String> ConditionMap
        {
            get
            {
                return this.myConditionMap;
            }
        }
        #endregion

        #region methods
        private async Task<Dictionary<int, string>> GetConditionsAsync()
        {
            Dictionary<int, string> dict = new Dictionary<int,string>();

            StorageFolder appFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            System.Diagnostics.Debug.WriteLine(appFolder.Path);

            StorageFolder resourcesFolder = await appFolder.GetFolderAsync("Resources");
            System.Diagnostics.Debug.WriteLine(resourcesFolder.Path);

            StorageFolder conditionsFolder = await resourcesFolder.GetFolderAsync("WeatherConditions");
            System.Diagnostics.Debug.WriteLine(conditionsFolder.Path);

            StorageFile conditionsFile = await conditionsFolder.GetFileAsync("conditions_de.txt");

            Stream r;

            try
            {
                var foo = await conditionsFile.OpenReadAsync();
                //var bar = foo.GetResults();
                r = foo.AsStreamForRead();
            }
            catch (Exception ex)
            {
                throw;
            }
            Stream s = r;

            using (StreamReader reader = new StreamReader(s))
            {
                while (!reader.EndOfStream)
                {
                    string[] parts = reader.ReadLine().Split('=');
                    dict.Add(Convert.ToInt32(parts[0]), parts[1]);
                }
            }

            return dict;
        }
        #endregion
    }
}