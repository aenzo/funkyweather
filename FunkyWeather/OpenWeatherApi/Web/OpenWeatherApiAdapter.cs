﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenWeatherApi.XmlClasses;
using Windows.UI.Core;

namespace OpenWeatherApi.Web
{
    /// <summary>
    /// Stellt Methoden zur Interoperabilität mit der OpenWeatherMap™ API zur Verfügung. 
    /// </summary>
    public class OpenWeatherApiAdapter
    {
        #region vars
        #region consts
        private const string WILDCARD_SEARCHTERM = "#SEARCHTERM";
        private const string URL_CITIES = "http://api.openweathermap.org/data/2.5/find?q=" + WILDCARD_SEARCHTERM + "&type=like&mode=xml&units=metric";
        private const string URL_CURRENT_BY_ID = "http://api.openweathermap.org/data/2.5/weather?id=" + WILDCARD_SEARCHTERM + "&mode=xml&units=metric";
        private const string URL_CURRENT_BY_NAME = "http://api.openweathermap.org/data/2.5/weather?q=" + WILDCARD_SEARCHTERM + "&mode=xml&units=metric";
        private const string URL_WEATHERDATA_7DAYS_DAILY = "http://api.openweathermap.org/data/2.5/forecast/daily?id=" + WILDCARD_SEARCHTERM + "&mode=xml&units=metric&cnt=7";
        private const string URL_WEATHERDATA_6DAYS_HOURLY = "http://api.openweathermap.org/data/2.5/forecast?id=" + WILDCARD_SEARCHTERM + "&mode=xml&units=metric";
        private const string PARAM_APPID = "&APPID=6325999919624137489d939dab07ca6f";
        #endregion
        #endregion
        
        #region events
        /// <summary>
        /// Tritt ein, wenn der asynchrone Vorgang beim Abruf der OpenWeatherMap™ API fertiggestellt wurde.
        /// </summary>
        public event GetCitiesAsyncCompletedEventHandler GetCitiesAsyncCompleted;

        /// <summary>
        /// Tritt ein, wenn der asynchrone Vorgang feschschlägt.
        /// </summary>
        public event GetCitiesAsyncFailedEventHandler GetCitiesAsyncFailed;

        /// <summary>
        /// Tritt ein, wenn der asynchrone Vorgang beim Abruf der OpenWeatherMap™ API fertiggestellt wurde.
        /// </summary>
        public event GetCurrentAsyncCompletedEventHandler GetCurrentAsyncCompleted;

        /// <summary>
        /// Tritt ein, wenn der asynchrone Vorgang feschschlägt.
        /// </summary>
        public event GetCurrentAsyncFailedEventHandler GetCurrentAsyncFailed;

        /// <summary>
        /// Tritt ein, wenn der asynchrone Vorgang beim Abruf der OpenWeatherMap™ API fertiggestellt wurde.
        /// </summary>
        public event GetWeatherDataAsyncCompletedEventHandler GetWeatherDataAsyncCompleted;

        /// <summary>
        /// Tritt ein, wenn der asynchrone Vorgang fehlschlägt.
        /// </summary>
        public event GetWeatherDataAsyncFailedEventHandler GetWeatherDataAsyncFailed;
        #endregion

        #region methods
        #region private
        /// <summary>
        /// Ruft einen URL ab und gibt die Antwort als String zurück.
        /// </summary>
        /// <param name="Url">Der URL, der abgerufen werden soll</param>
        /// <returns>Der abgerufene Inhalt als String</returns>
        /// <exception cref="System.Net.WebException"/>
        private async Task<String> GetResponseString(String Url)
        {
            WebRequest req = WebRequest.Create(Url + PARAM_APPID);

#if DEBUG
            System.Diagnostics.Debug.WriteLine(Url + PARAM_APPID);
#endif

            string res = String.Empty;

            try
            {
                WebResponse resp = await req.GetResponseAsync();
                res = new StreamReader(resp.GetResponseStream()).ReadToEnd();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Fehler bei WebRequest.GetResponse()");
                throw new WebException("Der gewünschte Inhalt konnte nicht abgerufen werden. (" + ex.Message + ")");
            }

            if (String.IsNullOrWhiteSpace(res))
            {
                throw new WebException("Die Server hat eine leere Antwort gesendet.");
            }

            return res;
        }
        #endregion

        #region public
        //Cities

        /// <summary>
        /// Schickt eine Suchanfrage an die OpenWeatherMap™ API und sucht nach Städten, deren Namen dem angegebenen Suchbegriff ähneln.
        /// </summary>
        /// <param name="CityName">Der Name der zu suchenden Stadt</param>
        /// <returns>Eine Liste von Städten, deren Namen dem Suchbegriff ähneln</returns>
        /// <exception cref="System.Net.WebException"/>
        public IList<CityFeedEntry> GetCities(String CityName)
        {
            string responseString = String.Empty;

            responseString = GetResponseString(URL_CITIES.Replace(WILDCARD_SEARCHTERM, CityName)).Result;

            CityFeed f = CityFeed.FromXmlString(responseString);

            if (f == null)
            {
                return null;
            }

            IList<CityFeedEntry> list = f.Items;

            return list;
        }

        /// <summary>
        /// Schickt eine asynchrone Suchanfrage an die OpenWeatherMap™ API und sucht nach Städten, deren Namen dem angegebenen Suchbegriff ähneln.
        /// </summary>
        /// <param name="CityName">Der Name der zu suchenden Stadt</param>
        public void GetCitiesAsync(String CityName)
        {
            CoreDispatcher disp = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;

            Task.Run(() =>
            {
                IList<CityFeedEntry> entries;
                try
                {
                    entries = GetCities(CityName);

                    disp.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                        new DispatchedHandler(() =>
                        {
                            this.GetCitiesAsyncCompleted(this, new GetCitiesAsyncCompletedEventArgs(entries));
                        }));
                }
                catch (Exception)
                {
                    disp.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, new DispatchedHandler(() =>
                    {
                        this.GetCitiesAsyncFailed(this, new GetCitiesAsyncFailedEventArgs(CityName));
                    }));
                }
            });
        }

        //Current

        /// <summary>
        /// Schickt eine Suchanfrage an die OpenWeatherMap™ API und ruft die aktuellen Wetterdaten für die angegebene Stadt ab.
        /// </summary>
        /// <param name="CityName">Der Name der Stadt, für die die Wetterdaten abgerufen werden sollen</param>
        /// <returns>Die aktuellen Wetterdaten für die Stadt mit dem angegebenen Namen</returns>
        public CurrentCity GetCurrentByName(String CityName)
        {
            string responseString = String.Empty;

            responseString = GetResponseString(URL_CURRENT_BY_NAME.Replace(WILDCARD_SEARCHTERM, CityName)).Result;

            CurrentCity c = CurrentCity.FromXmlString(responseString);

            return c;
        }

        /// <summary>
        /// Schickt eine asynchrone Suchanfrage an die OpenWeatherMap™ API und ruft die aktuellen Wetterdaten für die angegebene Stadt ab.
        /// </summary>
        /// <param name="CityName">Der Name der Stadt, für die die Wetterdaten abgerufen werden sollen</param>
        public void GetCurrentByNameAsync(String CityName)
        {
            CoreDispatcher disp = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;

            Task.Run(() =>
            {
                CurrentCity current;

                try
                {
                    current = GetCurrentByName(CityName);

                    disp.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, new DispatchedHandler(() =>
                    {
                        this.GetCurrentAsyncCompleted(this, new GetCurrentAsyncCompletedEventArgs(current));
                    }));
                }
                catch (Exception)
                {
                    disp.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, new DispatchedHandler(() =>
                    {
                        this.GetCurrentAsyncFailed(this, new GetCurrentAsyncFailedEventArgs(CityName));
                    }));
                }
            });
        }

        /// <summary>
        /// Schickt eine Suchanfrage an die OpenWeatherMap™ API und ruft die aktuellen Wetterdaten für die angegebene Stadt ab.
        /// </summary>
        /// <param name="CityId">Die ID der Stadt, für die die Wetterdaten abgerufen werden sollen</param>
        /// <returns>Die aktuellen Wetterdaten für die Stadt mit der angegebenen ID</returns>
        public CurrentCity GetCurrentByID(String CityId)
        {
            string responseString = String.Empty;

            responseString = GetResponseString(URL_CURRENT_BY_ID.Replace(WILDCARD_SEARCHTERM, CityId)).Result;

            CurrentCity c = CurrentCity.FromXmlString(responseString);

            return c;
        }

        /// <summary>
        /// Schickt eine asynchrone Suchanfrage an die OpenWeatherMap™ API und ruft die aktuellen Wetterdaten für die angegebene Stadt ab.
        /// </summary>
        /// <param name="CityId">Die ID der Stadt, für die die Wetterdaten abgerufen werden sollen</param>
        public void GetCurrentByIdAsync(String CityId)
        {
            CoreDispatcher disp = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;

            Task.Run(() =>
            {
                CurrentCity current;

                try
                {
                    current = GetCurrentByID(CityId);

                    disp.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, new DispatchedHandler(() =>
                    {
                        this.GetCurrentAsyncCompleted(this, new GetCurrentAsyncCompletedEventArgs(current));
                    }));
                }
                catch (Exception)
                {
                    disp.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, new DispatchedHandler(() =>
                    {
                        this.GetCurrentAsyncFailed(this, new GetCurrentAsyncFailedEventArgs(CityId));
                    }));
                }

            });
        }

        //WeatherData

        /// <summary>
        /// Schickt eine Suchanfrage an die OpenWeatherMap™ und ruft die 6-Tage-Wettervorhersage mit 3-Stunden-Intervallen für die angegebene Stadt ab.
        /// </summary>
        /// <param name="CityId">Die ID der Stadt, für die die Wettervorhersage abgerufen werden soll</param>
        /// <returns>Die 6-Tage-Wettervorhersage mit 3-Stunden-Intervallen für die Stadt mit der angegebenen ID</returns>
        public DetailedWeather Get6DayForecastById(String CityId)
        {
            string responseString = String.Empty;

            responseString = GetResponseString(URL_WEATHERDATA_6DAYS_HOURLY.Replace(WILDCARD_SEARCHTERM, CityId)).Result;

            DetailedWeather d = DetailedWeather.FromXmlString(responseString);

            return d;
        }

        /// <summary>
        /// Schickt eine Suchanfrage an die OpenWeatherMap™ API und ruft die 7-Tage-Wettervorhersage für die angegebene Stadt ab.
        /// </summary>
        /// <param name="CityId">Die ID der Stadt, für die die Wettervorhersage abgerufen werden soll</param>
        /// <returns>Die 7-Tage-Wettervorhersage für die Stadt mit der angegebenen ID</returns>
        public DetailedWeather Get7DayForecastById(String CityId)
        {
            string responseString = String.Empty;

            responseString = GetResponseString(URL_WEATHERDATA_7DAYS_DAILY.Replace(WILDCARD_SEARCHTERM, CityId)).Result;

            DetailedWeather d = DetailedWeather.FromXmlString(responseString);

            return d;
        }

        /// <summary>
        /// Schickt eine asynchrone Suchanfrage an die OpenWeatherMap™ API und ruft die 6-Tage-Vorhersage mit 3-Stunden-Intervallen für die angegebene Stadt ab.
        /// </summary>
        /// <param name="CityId">Die ID der Stadt, für die die Wettervorhersage abgerufen werden soll</param>
        public void Get6DayForecastByIdAsync(String CityId)
        {
            CoreDispatcher disp = CoreWindow.GetForCurrentThread().Dispatcher;

            Task.Run(() =>
                {
                    DetailedWeather data;

                    try
                    {
                        data = Get6DayForecastById(CityId);

                        disp.RunAsync(CoreDispatcherPriority.Normal,
                            new DispatchedHandler(() =>
                                {
                                    this.GetWeatherDataAsyncCompleted(this, new GetWeatherDataAsyncCompletedEventArgs(data));
                                }));
                    }
                    catch (Exception)
                    {
                        disp.RunAsync(CoreDispatcherPriority.Normal,
                            new DispatchedHandler(() =>
                                {
                                    this.GetWeatherDataAsyncFailed(this, new GetWeatherDataAsyncFailedEventArgs(CityId));
                                }));
                    }
                });
        }

        /// <summary>
        /// Schickt eine asynchrone Suchanfrage an die OpenWeatherMap™ API und ruft die 7-Tage-Wettervorhersage für die angegebene Stadt ab.
        /// </summary>
        /// <param name="CityId">Die ID der Stadt, für die die Wettervorhersage abgerufen werden soll</param>
        public void Get7DayForecastByIdAsync(String CityId)
        {
            CoreDispatcher disp = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;

            Task.Run(() =>
            {
                DetailedWeather data;

                try
                {
                    data = Get7DayForecastById(CityId);

                    disp.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                        new DispatchedHandler(() =>
                        {
                            this.GetWeatherDataAsyncCompleted(this, new GetWeatherDataAsyncCompletedEventArgs(data));
                        }));
                }
                catch (Exception)
                {
                    disp.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                        new DispatchedHandler(() =>
                        {
                            this.GetWeatherDataAsyncFailed(this, new GetWeatherDataAsyncFailedEventArgs(CityId));
                        }));
                }
            });
        }
        #endregion
        #endregion
    }
}
