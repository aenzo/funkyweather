﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWeatherApi.Web
{
    /// <summary>
    /// Tritt ein, wenn der asynchrone Vorgang beim Abruf der OpenWeatherMap™ API fertiggestellt wurde.
    /// </summary>
    /// <param name="sender">Das aufrufende Objekt</param>
    /// <param name="e">EventArgs - Enthält das Ergebnis des asynchronen Aufrufs</param>
    public delegate void GetWeatherDataAsyncCompletedEventHandler(object sender, GetWeatherDataAsyncCompletedEventArgs e);
}
