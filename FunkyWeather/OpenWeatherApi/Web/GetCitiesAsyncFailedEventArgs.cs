﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenWeatherApi.XmlClasses;

namespace OpenWeatherApi.Web
{
    /// <summary>
    /// EventArgs - Enthält das Ergebnis des asynchronen Aufrufs.
    /// </summary>
    public class GetCitiesAsyncFailedEventArgs : EventArgs
    {
        private string mySearchTerm;

        /// <summary>
        /// Erstellt eine neue instanz der GetCitiesAsyncCompletedEventArgs-Klasse.
        /// </summary>
        /// <param name="SearchTerm">Der Parameter des Aufrufs</param>
        public GetCitiesAsyncFailedEventArgs(String SearchTerm)
        {
            this.mySearchTerm = SearchTerm;
        }

        /// <summary>
        /// Enthält den Parameter des Aufrufs, der fehlgeschlug.
        /// </summary>
        public String SearchTerm
        {
            get
            {
                return this.mySearchTerm;
            }
        }
    }
}
