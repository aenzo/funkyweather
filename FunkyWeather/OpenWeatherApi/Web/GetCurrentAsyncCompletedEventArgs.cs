﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenWeatherApi.XmlClasses;

namespace OpenWeatherApi.Web
{
    /// <summary>
    /// EventArgs - Enthält das Ergebnis des asynchronen Aufrufs.
    /// </summary>
    public class GetCurrentAsyncCompletedEventArgs : EventArgs
    {
        private CityFeedEntry myCitiesItem;

        /// <summary>
        /// Erstellt eine neue instanz der GetCurrentAsyncCompletedEventArgs-Klasse.
        /// </summary>
        /// <param name="CitiesItem">Das CitiesEntry-Objekt, das vom asynchronen Aufruf zurückgegeben werden soll.</param>
        public GetCurrentAsyncCompletedEventArgs(CityFeedEntry CitiesItem)
        {
            this.myCitiesItem = CitiesItem;
        }

        /// <summary>
        /// Enthält das CitiesItem-Objekt, das vom asynchronen Aufruf zurückgegeben werden soll.
        /// </summary>
        public CityFeedEntry CitiesItem
        {
            get
            {
                return this.myCitiesItem;
            }
        }
    }
}
