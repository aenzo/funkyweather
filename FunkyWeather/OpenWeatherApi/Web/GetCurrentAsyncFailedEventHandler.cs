﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWeatherApi.Web
{
    /// <summary>
    /// Tritt ein, wenn der asynchrone Vorgang beim Abruf der OpenWeatherMap™ API fehlschlägt.
    /// </summary>
    /// <param name="sender">Das aufrufende Objekt</param>
    /// <param name="e">EventArgs - Enthält den Parameter des Aufrufs</param>
    public delegate void GetCurrentAsyncFailedEventHandler(object sender, GetCurrentAsyncFailedEventArgs e);
}
