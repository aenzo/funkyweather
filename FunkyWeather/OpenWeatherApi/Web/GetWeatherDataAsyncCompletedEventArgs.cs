﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenWeatherApi.XmlClasses;

namespace OpenWeatherApi.Web
{
    /// <summary>
    /// EventArgs - Enthält das Ergebnis des asynchronen Aufrufs.
    /// </summary>
    public class GetWeatherDataAsyncCompletedEventArgs : EventArgs
    {
        private DetailedWeather myWeatherData;

        /// <summary>
        /// Erstellt eine neue instanz der GetWeatherDataAsyncCompletedEventArgs-Klasse.
        /// </summary>
        /// <param name="WeatherData">Das WeatherData-Objekt, das vom asynchronen Aufruf zurückgegeben werden soll.</param>
        public GetWeatherDataAsyncCompletedEventArgs(DetailedWeather WeatherData)
        {
            this.myWeatherData = WeatherData;
        }

        /// <summary>
        /// Enthält das WeatherData-Objekt, das vom asynchronen Aufruf zurückgegeben werden soll.
        /// </summary>
        public DetailedWeather WeatherData
        {
            get
            {
                return this.myWeatherData;
            }
        }
    }
}
