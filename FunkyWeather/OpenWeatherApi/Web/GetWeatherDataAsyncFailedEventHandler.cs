﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWeatherApi.Web
{
    /// <summary>
    /// Tritt ein, wenn der asynchrone Vorgang beim Abruf der OpenWeatherMap™ API fehlschlägt.
    /// <param name="sender">Das aufrufende Objekt</param>
    /// <param name="e">EventArgs - Enthält den Parameter des Aufruf.</param>
    public delegate void GetWeatherDataAsyncFailedEventHandler(object sender, GetWeatherDataAsyncFailedEventArgs e);
}
