﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenWeatherApi.XmlClasses;

namespace OpenWeatherApi.Web
{
    /// <summary>
    /// EventArgs - Enthält das Ergebnis des asynchronen Aufrufs.
    /// </summary>
    public class GetCitiesAsyncCompletedEventArgs : EventArgs
    {
        private IList<CityFeedEntry> myCities;

        /// <summary>
        /// Erstellt eine neue instanz der GetCitiesAsyncCompletedEventArgs-Klasse.
        /// </summary>
        /// <param name="Cities">Das CitiesEntry-Objekt, das vom asynchronen Aufruf zurückgegeben werden soll.</param>
        public GetCitiesAsyncCompletedEventArgs(IList<CityFeedEntry> Cities)
        {
            this.myCities = Cities;
        }

        /// <summary>
        /// Enthält das Cities-Objekt, das vom asynchronen Aufruf zurückgegeben werden soll.
        /// </summary>
        public IList<CityFeedEntry> Cities
        {
            get
            {
                return this.myCities;
            }
        }
    }
}
