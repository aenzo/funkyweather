﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenWeatherApi.XmlClasses;

namespace OpenWeatherApi.Web
{
    /// <summary>
    /// EventArgs - Enthält den Parameter des Aufrufs.
    /// </summary>
    public class GetWeatherDataAsyncFailedEventArgs : EventArgs
    {
        private string mySearchTerm;

        /// <summary>
        /// Erstellt eine neue instanz der GetWeatherDataAsyncFailedEventArgs-Klasse.
        /// </summary>
        /// <param name="SearchTerm">Der Parameter des Aufrufs</param>
        public GetWeatherDataAsyncFailedEventArgs(String SearchTerm)
        {
            this.mySearchTerm = SearchTerm;
        }

        /// <summary>
        /// Enthält den Parameter des Aufrufs, der fehlschlug.
        /// </summary>
        public String SearchTerm
        {
            get
            {
                return this.mySearchTerm;
            }
        }
    }
}
