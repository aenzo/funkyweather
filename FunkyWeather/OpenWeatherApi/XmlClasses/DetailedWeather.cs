﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Enthält Wetterdaten, Temperaturangaben und die Sonnendauer für eine bestimmte Stadt.
    /// </summary>
    [XmlRoot("weatherdata")]
    public class DetailedWeather
    {
        #region vars
        private Forecast[] myForecast;
        private Location myLocation;
        private Sun mySun;
        #endregion

        #region props

        /// <summary>
        /// Gibt die Wettervorhersage für einen bestimmten Tag an.
        /// </summary>
        [XmlArray("forecast")]
        [XmlArrayItem("time")]
        public Forecast[] Forecast
        {
            get
            {
                return this.myForecast;
            }
            set
            {
                this.myForecast = value;
            }
        }

        /// <summary>
        /// Gibt die Stadt an, für die die Wettervorhersage gilt.
        /// </summary>
        [XmlElement("location")]
        public Location Location
        {
            get
            {
                return this.myLocation;
            }
            set
            {
                this.myLocation = value;
            }
        }

        /// <summary>
        /// Enthält Informationen über die Sonnenstunden für die angegebene Stadt.
        /// </summary>
        [XmlElement("sun")]
        public Sun Sun
        {
            get
            {
                return this.mySun;
            }
            set
            {
                this.mySun = value;
            }
        }
        #endregion

        #region methods
        /// <summary>
        /// Gibt ein WeatherData-Objekt zurück, das im angegebenen Xml-String serialisiert abgebildet wird.
        /// </summary>
        /// <param name="Xml">Das zu deserialisierende WeatherData-Xml</param>
        /// <returns>Eine WeatherData-Instanz, die das serialisierte Objekt im angegebenen Xml-String abbildet</returns>
        public static DetailedWeather FromXmlString(String Xml)
        {
            XmlReader reader = XmlReader.Create(new StringReader(Xml));
            XmlSerializer ser = new XmlSerializer(typeof(DetailedWeather));

            DetailedWeather foo = null;

            try
            {
                foo = (DetailedWeather)ser.Deserialize(reader);
            }
            finally { }

            return foo;
        }
        #endregion
    }
}
