﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Enthält die Wettervorhersage für einen bestimmten Tag.
    /// </summary>
    public class Forecast
    {
        #region vars
        private Clouds myClouds;
        private DateTime myDay;
        private DateTime myFrom;
        private Humidity myHumidity;
        private Pressure myPressure;
        private DetailedTemperature myTemperature;
        private DateTime myTo;
        private Symbol mySymbol;
        private WindDirection myWindDirection;
        private WindSpeed myWindSpeed;
        #endregion

        #region props
        /// <summary>
        /// Beinhaltet Informationen über die Bewölkung.
        /// </summary>
        [XmlElement("clouds")]
        public Clouds Clouds
        {
            get
            {
                return this.myClouds;
            }
            set
            {
                this.myClouds = value;
            }
        }

        /// <summary>
        /// Der Tag, für den die Wettervorhersage gilt (bei täglicher Vorhersage).
        /// </summary>
        [XmlAttribute("day")]
        public DateTime Day
        {
            get
            {
                return this.myDay;
            }
            set
            {
                this.myDay = value;
            }
        }

        /// <summary>
        /// Gibt an, ab wann die Vorhersage gilt (bei 3-stündiger Vorhersage).
        /// </summary>
        [XmlAttribute("from")]
        public DateTime From
        {
            get
            {
                return this.myFrom;
            }
            set
            {
                this.myFrom = value;
            }
        }

        /// <summary>
        /// Gibt Informationen über die Luftfeuchtigkeit an.
        /// </summary>
        [XmlElement("humidity")]
        public Humidity Humidity
        {
            get
            {
                return this.myHumidity;
            }
            set
            {
                this.myHumidity = value;
            }
        }

        /// <summary>
        /// Gibt Informationen über den Luftdruck an.
        /// </summary>
        [XmlElement("pressure")]
        public Pressure Pressure
        {
            get
            {
                return this.myPressure;
            }
            set
            {
                this.myPressure = value;
            }
        }

        /// <summary>
        /// Die Temperaturen, die an dem Tag herrschen, für den die Wettervorhersage gilt.
        /// </summary>
        [XmlElement("temperature")]
        public DetailedTemperature Temperature
        {
            get
            {
                return this.myTemperature;
            }
            set
            {
                this.myTemperature = value;
            }
        }

        /// <summary>
        /// Gibt an, bis wann die Vorhersage gilt (bei 3-stündiger Vorhersage).
        /// </summary>
        [XmlAttribute("to")]
        public DateTime To
        {
            get
            {
                return this.myTo;
            }
            set
            {
                this.myTo = value;
            }
        }

        /// <summary>
        /// Gibt Informationen über das Symbol an, das die Wettervorhersage am besten beschreibt.
        /// </summary>
        [XmlElement("symbol")]
        public Symbol Symbol
        {
            get
            {
                return this.mySymbol;
            }
            set
            {
                this.mySymbol = value;
            }
        }

        /// <summary>
        /// Gibt Informationen über die Windrichtung an.
        /// </summary>
        [XmlElement("windDirection")]
        public WindDirection WindDirection
        {
            get
            {
                return this.myWindDirection;
            }
            set
            {
                this.myWindDirection = value;
            }
        }

        /// <summary>
        /// Gibt Informationen über die Windgeschwindigkeit an.
        /// </summary>
        [XmlElement("windSpeed")]
        public WindSpeed WindSpeed
        {
            get
            {
                return this.myWindSpeed;
            }
            set
            {
                this.myWindSpeed = value;
            }
        }
        #endregion
    }
}
