﻿using System;
using System.Text;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Enthält Informationen über Sonnenstunden.
    /// </summary>
    public class Sun
    {
        #region vars
        private DateTime myRise;
        private DateTime mySet;
        #endregion

        #region props
        /// <summary>
        /// Gibt die Uhrzeit für den Sonnenaufgang an angegebenen Tag an.
        /// </summary>
        [XmlAttribute("rise")]
        public DateTime Sunrise
        {
            get
            {
                return this.myRise;
            }
            set
            {
                this.myRise = value;
            }
        }

        /// <summary>
        /// Gibt die Uhrzeit für den Sonnenuntergang an angegebenen Tag an.
        /// </summary>
        [XmlAttribute("set")]
        public DateTime Sunset
        {
            get
            {
                return this.mySet;
            }
            set
            {
                this.mySet = value;
            }
        }
        #endregion
    }
}
