﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Beinhaltet Informationen über die Luftdruckverhältnisse.
    /// </summary>
    public class Pressure
    {
        #region vars
        private string myUnit;
        private double myValue;
        #endregion

        #region props
        /// <summary>
        /// Gibt die Einheit an, in der der Luftdruck unter Pressure.Value angegeben wird.
        /// </summary>
        [XmlAttribute("unit")]
        public String Unit
        {
            get
            {
                return this.myUnit;
            }
            set
            {
                this.myUnit = value;
            }
        }
        
        /// <summary>
        /// Gibt den Wert des Luftdrucks an.
        /// </summary>
        [XmlAttribute("value")]
        public Double Value
        {
            get
            {
                return this.myValue;
            }
            set
            {
                this.myValue = value;
            }
        }
        #endregion
    }
}
