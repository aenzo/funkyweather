﻿using System;
using System.Text;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Gibt die Stadt an, für die die Wettervorhersage gilt.
    /// </summary>
    public class Location
    {
        #region vars
        private string myCountry;
        private string myName;
        #endregion

        #region props
        /// <summary>
        /// Das Kürzel des Landes, in dem sich die Stadt befindet.
        /// </summary>
        [XmlElement("country")]
        public String Country
        {
            get
            {
                return this.myCountry;
            }
            set
            {
                this.myCountry = value;
            }
        }

        /// <summary>
        /// Der Name der Stadt.
        /// </summary>
        [XmlElement("name")]
        public String Name
        {
            get
            {
                return this.myName;
            }
            set
            {
                this.myName = value;
            }
        }
        #endregion
    }
}
