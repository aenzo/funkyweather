﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Bildet die aktuellen Wetterdaten für eine Stadt ab.
    /// </summary>
    [XmlRoot("current")]
    public class CurrentCity : CityFeedEntry
    {
        #region methods
        /// <summary>
        /// Gibt ein CurrentCity-Objekt zurück, das im angegebenen Xml-String serialisiert abgebildet wird.
        /// </summary>
        /// <param name="Xml">Das zu deserialisierende CurrentCity-Xml</param>
        /// <returns>Eine CurrentCity-Instanz, die das serialisierte Objekt im angegebenen Xml-String abbildet</returns>
        public static CurrentCity FromXmlString(String Xml)
        {
            XmlReader reader = XmlReader.Create(new StringReader(Xml));
            XmlSerializer ser = new XmlSerializer(typeof(CurrentCity));

            CurrentCity foo = null;

            try
            {
                foo = (CurrentCity)ser.Deserialize(reader);
            }
            catch
            {
                foo = null;
            }

            return foo;
        }
        #endregion
    }
}
