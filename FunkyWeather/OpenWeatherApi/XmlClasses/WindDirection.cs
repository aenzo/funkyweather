﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Beinhaltet Informationen über die Windrichtung.
    /// </summary>
    public class WindDirection
    {
        #region vars
        private double myDeg;
        private string myDirection;
        #endregion

        #region props
        /// <summary>
        /// Gibt die Richtung als Winkel an..
        /// </summary>
        [XmlAttribute("deg")]
        public Double Degrees
        {
            get
            {
                return this.myDeg;
            }
            set
            {
                this.myDeg = value;
            }
        }
        
        /// <summary>
        /// Gibt die Richtung als Code an.
        /// </summary>
        [XmlAttribute("code")]
        public String Direction
        {
            get
            {
                return this.myDirection;
            }
            set
            {
                this.myDirection = value;
            }
        }
        #endregion
    }
}
