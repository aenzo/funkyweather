﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Gibt Informationen über das Symbol an, das eine Wettervorhersage beschreibt.
    /// </summary>
    public class Symbol
    {
        #region vars
        private int myNumber;
        private string myFileName;
        #endregion

        #region props
        /// <summary>
        /// Die OpenWeatherApi™-interne Symbol-ID.
        /// </summary>
        [XmlAttribute("number")]
        public Int32 Number
        {
            get
            {
                return this.myNumber;
            }
            set
            {
                this.myNumber = value;
            }
        }

        /// <summary>
        /// Der OpenWeatherApi™-interne Dateiname für das Symbol (kann vom Server abgerufen werden).
        /// </summary>
        [XmlAttribute("var")]
        public String FileName
        {
            get
            {
                return this.myFileName;
            }
            set
            {
                this.myFileName = value;
            }
        }
        #endregion
    }
}
