﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Stellt einen Feed mit CitiesEntry-Einträgen dar, der das Suchergebnis einer Städtesuche abbildet.
    /// </summary>
    [XmlRoot("cities")]
    public class CityFeed
    {
        #region vars
        private List<CityFeedEntry> myItems;
        #endregion

        #region props
        /// <summary>
        /// Enthält die CitiesEntry-Einträge aller Städte, die zur Suche gefunden wurden.
        /// </summary>
        [XmlArray("list")]
        [XmlArrayItem("item")]
        public List<CityFeedEntry> Items
        {
            get
            {
                return this.myItems;
            }
            set
            {
                this.myItems = value;
            }
        }
        #endregion

        #region methods
        /// <summary>
        /// Gibt ein CitiesFeed-Objekt zurück, das im angegebenen Xml-String serialisiert abgebildet wird.
        /// </summary>
        /// <param name="Xml">Das zu deserialisierende CitiesFeed-Xml</param>
        /// <returns>Eine CitiesFeed-Instanz, die das serialisierte Objekt im angegebenen Xml-String abbildet</returns>
        public static CityFeed FromXmlString(String Xml)
        {
            XmlReader reader = XmlReader.Create(new StringReader(Xml));
            XmlSerializer ser = new XmlSerializer(typeof(CityFeed));

            CityFeed foo;

            try
            {
                foo = (CityFeed)ser.Deserialize(reader);
            }
            catch
            {
                foo = null;
            }

            return foo;
        }
        #endregion
    }
}
