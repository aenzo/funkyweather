﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Bildet einen Eitrag des CitiesFeeds ab.
    /// </summary>
    public class CityFeedEntry
    {
        #region vars
        private City myCity;
        private SimpleTemperature myTemperature;
        private SimpleWeather myWeather;
        #endregion

        #region props
        /// <summary>
        /// Enthält Informationen über die Stadt, die zum CitiesEntry gehört.
        /// </summary>
        [XmlElement("city")]
        public City City
        {
            get
            {
                return this.myCity;
            }
            set
            {
                this.myCity = value;
            }
        }

        /// <summary>
        /// Enthält die Temperaturdaten des CitiesEntries.
        /// </summary>
        [XmlElement("temperature")]
        public SimpleTemperature Temperature
        {
            get
            {
                return this.myTemperature;
            }
            set
            {
                this.myTemperature = value;
            }
        }

        /// <summary>
        /// Enthält die Wetterbeschreibung und -ID des CitiesEntries.
        /// </summary>
        [XmlElement("weather")]
        public SimpleWeather Weather
        {
            get
            {
                return this.myWeather;
            }
            set
            {
                this.myWeather = value;
            }
        }
        #endregion
    }
}
