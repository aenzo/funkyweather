﻿using System;
using System.Text;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Enthält Temperaturdaten für verschiedene Tageszeiten.
    /// </summary>
    public class DetailedTemperature
    {
        #region vars
        private double myDay;
        private double myEvening;
        private double myMaximum;
        private double myMinium;
        private double myMorning;
        private double myNight;
        private double myValue;
        #endregion

        #region props
        /// <summary>
        /// Gibt die Temperatur an, die tagsüber herrscht.
        /// </summary>
        [XmlAttribute("day")]
        public Double Day
        {
            get
            {
                return this.myDay;
            }
            set
            {
                this.myDay = value;
            }
        }

        /// <summary>
        /// Gibt die Temperatur an, die abends herrscht.
        /// </summary>
        [XmlAttribute("eve")]
        public Double Evening
        {
            get
            {
                return this.myEvening;
            }
            set
            {
                this.myEvening = value;
            }
        }

        /// <summary>
        /// Gibt die Temperatur an, die morgens herrscht.
        /// </summary>
        [XmlAttribute("morn")]
        public Double Morning
        {
            get
            {
                return this.myMorning;
            }
            set
            {
                this.myMorning = value;
            }
        }

        /// <summary>
        /// Gibt die Temperatur an, die nachts herrscht.
        /// </summary>
        [XmlAttribute("night")]
        public Double Night
        {
            get
            {
                return this.myNight;
            }
            set
            {
                this.myNight = value;
            }
        }

        /// <summary>
        /// Gibt die Maximaltemperatur an.
        /// </summary>
        [XmlAttribute("max")]
        public Double Maximum
        {
            get
            {
                return this.myMaximum;
            }
            set
            {
                this.myMaximum = value;
            }
        }

        /// <summary>
        /// Gibt die Minimaltemperatur an.
        /// </summary>
        [XmlAttribute("min")]
        public Double Minimum
        {
            get
            {
                return this.myMinium;
            }
            set
            {
                this.myMinium = value;
            }
        }

        /// <summary>
        /// Gibt die aktuelle Temperatur an (bei 3-stündiger Vorhersage).
        /// </summary>
        [XmlAttribute("value")]
        public Double Value
        {
            get
            {
                return this.myValue;
            }
            set
            {
                this.myValue = value;
            }
        }
        #endregion
    }
}
