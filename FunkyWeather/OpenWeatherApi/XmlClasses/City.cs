﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Enthält Informationen über eine Stadt.
    /// </summary>
    public class City
    {
        #region vars
        private string myCountry;
        private string myId;
        private string myName;
        private Sun mySun;
        #endregion

        #region props
        /// <summary>
        /// Das Kürzel des Landes, in dem sich die Stadt befindet.
        /// </summary>
        [XmlElement("country")]
        public String Country
        {
            get
            {
                return this.myCountry;
            }
            set
            {
                this.myCountry = value;
            }
        }

        /// <summary>
        /// Die OpenWeatherMap™-interne ID der Stadt.
        /// </summary>
        [XmlAttribute("id")]
        public String Id
        {
            get
            {
                return this.myId;
            }
            set
            {
                this.myId = value;
            }
        }

        /// <summary>
        /// Der Name der Stadt.
        /// </summary>
        [XmlAttribute("name")]
        public String Name
        {
            get
            {
                return this.myName;
            }
            set
            {
                this.myName = value;
            }
        }

        /// <summary>
        /// Enthält Informationen über die Sonnenstunden für die aktuelle Stadt.
        /// </summary>
        [XmlElement("sun")]
        public Sun Sun
        {
            get
            {
                return this.mySun;
            }
            set
            {
                this.mySun = value;
            }
        }
        #endregion
    }
}
