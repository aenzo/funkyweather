﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Beinhaltet Informationen über die Luftfeuchtigkeit.
    /// </summary>
    public class Humidity
    {
        #region vars
        private string myUnit;
        private double myValue;
        #endregion

        #region props
        /// <summary>
        /// Gibt die Einheit an, in der die Luftfeuchtigkeit unter Humidity.Value angegeben wird.
        /// </summary>
        [XmlAttribute("unit")]
        public String Unit
        {
            get
            {
                return this.myUnit;
            }
            set
            {
                this.myUnit = value;
            }
        }
        
        /// <summary>
        /// Gibt den Wert der Luftfeuchtigkeit an an.
        /// </summary>
        [XmlAttribute("value")]
        public Double Value
        {
            get
            {
                return this.myValue;
            }
            set
            {
                this.myValue = value;
            }
        }
        #endregion
    }
}
