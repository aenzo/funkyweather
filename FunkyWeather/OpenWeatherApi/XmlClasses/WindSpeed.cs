﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Beinhaltet Informationen über die Windgeschwindigkeit.
    /// </summary>
    public class WindSpeed
    {
        #region vars
        private double myValue;
        #endregion

        #region props
        
        /// <summary>
        /// Gibt die Windgeschwindigkeit in m/s an.
        /// </summary>
        [XmlAttribute("mps")]
        public Double Value
        {
            get
            {
                return this.myValue;
            }
            set
            {
                this.myValue = value;
            }
        }
        #endregion
    }
}
