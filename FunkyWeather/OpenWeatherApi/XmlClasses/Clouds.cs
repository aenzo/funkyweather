﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Beinhaltet Informationen über die Bewölkung.
    /// </summary>
    public class Clouds
    {
        #region vars
        private string myUnit;
        private double myValue;
        #endregion

        #region props
        /// <summary>
        /// Gibt die Einheit an, in der die Bewölkung in Clouds.Value angegeben ist.
        /// </summary>
        [XmlAttribute("unit")]
        public String Unit
        {
            get
            {
                return this.myUnit;
            }
            set
            {
                this.myUnit = value;
            }
        }

        /// <summary>
        /// Gibt den Wert der Bewölkung an.
        /// </summary>
        [XmlAttribute("all")]
        public Double Value
        {
            get
            {
                return this.myValue;
            }
            set
            {
                this.myValue = value;
            }
        }
        #endregion
    }
}
