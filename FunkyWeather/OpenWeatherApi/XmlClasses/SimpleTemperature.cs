﻿using System;
using System.Text;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Bildet eine einfache Temperaturangabe ab.
    /// </summary>
    public class SimpleTemperature
    {
        #region vars
        private double myValue;
        #endregion

        #region props
        /// <summary>
        /// Der Temperatur-Wert in Grad Celsius
        /// </summary>
        [XmlAttribute("value")]
        public Double Value
        {
            get
            {
                return this.myValue;
            }
            set
            {
                this.myValue = value;
            }
        }
        #endregion
    }
}
