﻿using System;
using System.Text;
using System.Xml.Serialization;

namespace OpenWeatherApi.XmlClasses
{
    /// <summary>
    /// Bildet eine Wettersituation ab.
    /// </summary>
    public class SimpleWeather
    {
        #region vars
        private int myNumber;
        private string myValue;
        private string myIcon;
        #endregion

        #region props
        /// <summary>
        /// Die ID der Wettersituation - diese ruft in Verbindung mit der WeatherConditionCodes-Klasse die deutsche Bezeichnung ab.
        /// </summary>
        [XmlAttribute("number")]
        public Int32 Number
        {
            get
            {
                return this.myNumber;
            }
            set
            {
                this.myNumber = value;
            }
        }

        /// <summary>
        /// Die englische Bezeichnung der Wettersituation.
        /// </summary>
        [XmlAttribute("value")]
        public String Value
        {
            get
            {
                return this.myValue;
            }
            set
            {
                this.myValue = value;
            }
        }

        /// <summary>
        /// Der zum Wetter passende Symbolname ohne Endung.
        /// </summary>
        [XmlAttribute("icon")]
        public String Icon
        {
            get
            {
                return this.myIcon;
            }
            set
            {
                this.myIcon = value;
            }
        }
        #endregion
    }
}
