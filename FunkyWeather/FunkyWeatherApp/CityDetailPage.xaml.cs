﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using OpenWeatherApi.XmlClasses;
using OpenWeatherApi.WeatherConditions;
using OpenWeatherApi.Web;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace FunkyWeatherApp
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class CityDetailPage : Page
    {
        private DateTime myDate;
        private OpenWeatherApiAdapter myAdapter;
        private App myApp;

        public CityDetailPage()
        {
            this.InitializeComponent();

            this.myApp = (App)App.Current;

            this.myAdapter = new OpenWeatherApiAdapter();
            this.myAdapter.GetWeatherDataAsyncCompleted += myAdapter_GetWeatherDataAsyncCompleted;
            this.myAdapter.GetWeatherDataAsyncFailed += myAdapter_GetWeatherDataAsyncFailed;
        }

        void myAdapter_GetWeatherDataAsyncFailed(object sender, GetWeatherDataAsyncFailedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }

        void myAdapter_GetWeatherDataAsyncCompleted(object sender, GetWeatherDataAsyncCompletedEventArgs e)
        {
            if (e.WeatherData == null)
            {
                if (this.Frame.CanGoBack)
                {
                    this.Frame.GoBack();
                }
            }

            DetailedWeather dw = e.WeatherData;

            this.headline.Text = "Funky Details zu " + dw.Location.Name + ", " + dw.Location.Country;

            IList<Forecast> flist = (from Forecast f in dw.Forecast
                                     where f.From.ToString("dd.MM.yyyy") == this.myDate.ToString("dd.MM.yyyy")
                                     select f).ToList<Forecast>();

            #region now
            if (this.myDate.ToString("dd.MM.yyyy") == DateTime.Now.ToString("dd.MM.yyyy"))
            {

                Forecast now = (from Forecast f in flist
                                where f.From.Hour <= DateTime.Now.Hour && f.To.Hour >= DateTime.Now.Hour
                                select f).FirstOrDefault<Forecast>();

                if (now == null)
                {
                    if (this.Frame.CanGoBack)
                    {
                        this.Frame.GoBack();
                    }
                }

                //image
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.UriSource = new System.Uri("ms-appx:///Resources/Images/Icons/" + now.Symbol.FileName + ".png");
                Image i = new Image();
                i.MaxHeight = 128.0D;
                i.MaxWidth = 128.0D;
                i.Source = bitmapImage;
                i.Stretch = Stretch.Uniform;
                i.HorizontalAlignment = HorizontalAlignment.Left;
                i.Margin = new Thickness(20, 20, 20, 0);
                this.spLeft.Children.Add(i);

                //temperatur
                TextBlock text = new TextBlock();
                text.FontSize = 24.0;
                text.Margin = new Thickness(2);
                text.Text = this.myApp.WeatherConditionCodes.Conditions[now.Symbol.Number];
                this.spLeft.Children.Add(text);

                //temperatur
                text = new TextBlock();
                text.FontSize = 24.0;
                text.Margin = new Thickness(2);
                text.Text = "Aktuelle Temperatur " + now.Temperature.Value.ToString("##0.0") + "°C";
                this.spLeft.Children.Add(text);

                //wolken
                text = new TextBlock();
                text.FontSize = 18.0;
                text.Margin = new Thickness(2);
                text.Text = now.Clouds.Value.ToString("#0") + "% Bewölkt";
                this.spLeft.Children.Add(text);

                //luftfeuchtigkeit
                text = new TextBlock();
                text.FontSize = 18.0;
                text.Margin = new Thickness(2);
                text.Text = now.Humidity.Value.ToString("#0") + "% Luftfeuchtigkeit";
                this.spLeft.Children.Add(text);

                //wind
                text = new TextBlock();
                text.FontSize = 18.0;
                text.Margin = new Thickness(2);
                text.Text = "Windgeschwindigkeit: " + now.WindSpeed.Value.ToString("##0.0") + " m/s";
                this.spLeft.Children.Add(text);
                text = new TextBlock();
                text.FontSize = 18.0;
                text.Margin = new Thickness(2);
                text.Text = "Windrichtung: " + now.WindDirection.Direction;
                this.spLeft.Children.Add(text);
            }
            else
            {
                this.spLeft.Visibility = Visibility.Collapsed;
            }
            #endregion

            foreach (Forecast fore in flist)
            {
                #region forecast
                StackPanel sp = new StackPanel();
                sp.Orientation = Orientation.Horizontal;
                sp.Margin = new Thickness(5);

                //image
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.UriSource = new System.Uri("ms-appx:///Resources/Images/Icons/" + fore.Symbol.FileName + ".png");
                Image i = new Image();
                i.MaxHeight = 60.0D;
                i.MaxWidth = 60.0D;
                i.Source = bitmapImage;
                i.Stretch = Stretch.Uniform;
                i.HorizontalAlignment = HorizontalAlignment.Left;
                i.Margin = new Thickness(0, 0, 10, 0);
                sp.Children.Add(i);

                StackPanel spr = new StackPanel();

                //luftfeuchtigkeit
                TextBlock text = new TextBlock();
                text.FontSize = 18.0;
                text.Text = "Von " + fore.From.ToString("HH:mm") + " Uhr bis " + fore.To.ToString("HH:mm") + "Uhr";
                spr.Children.Add(text);

                sp.Children.Add(spr);
                spr.Orientation = Orientation.Vertical;

                //temperatur
                text = new TextBlock();
                text.FontSize = 18.0;
                text.Text = fore.Temperature.Value.ToString("##0.0") + "°C, " + fore.Clouds.Value.ToString("#0") + "% Bewölkt";
                spr.Children.Add(text);

                this.spRight.Children.Add(sp);
                #endregion
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.mainGrid.Background = this.myApp.GetBackgroundBrush();

            if ((e.Parameter is DateTime))
            {
                this.myDate = (DateTime)e.Parameter;
                this.myAdapter.Get6DayForecastByIdAsync(this.myApp.CityId);
                this.headline.Text = "Funky Details";
            }
            else
            {
                if (this.Frame.CanGoBack)
                {
                    this.Frame.GoBack();
                }
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }
    }
}
