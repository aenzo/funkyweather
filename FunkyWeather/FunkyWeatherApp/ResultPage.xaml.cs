﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using OpenWeatherApi.XmlClasses;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace FunkyWeatherApp
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class ResultPage : Page
    {
        private App myApp;

        public ResultPage()
        {
            this.InitializeComponent();

            this.myApp = (App)App.Current;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.mainGrid.Background = this.myApp.GetBackgroundBrush();
            if (e.Parameter is IList<CityFeedEntry>)
            {
                this.mySearchPanel.Children.Clear();

                foreach (CityFeedEntry entry in (e.Parameter as IList<CityFeedEntry>))
                {
                    Button b = new Button();
                    b.Tag = entry.City.Id;
                    b.Content = entry.City.Name + ", " + entry.City.Country + " | " +
                        entry.Temperature.Value.ToString("##0.0") + "°C";
                    b.Margin = new Thickness(20.0, 0, 0, 5.0);
                    b.BorderThickness = new Thickness(0.0);
                    b.FontSize = 24.0;
                    b.Click += (sender, senderE) =>
                    {
                        this.myApp.CityId = (string)((sender as Button).Tag);
                        this.Frame.Navigate(typeof(MainPage), this.myApp.CityId);
                    };
                    this.mySearchPanel.Children.Add(b);
                }
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }
    }
}
