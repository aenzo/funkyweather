﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using OpenWeatherApi.XmlClasses;
using OpenWeatherApi.Web;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace FunkyWeatherApp
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class SearchPage : Page
    {
        OpenWeatherApiAdapter myAdapter;
        private App myApp;

        public SearchPage()
        {
            this.InitializeComponent();

            this.myProgressLabel.Visibility = this.myProgressRing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.txtFail.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            this.myApp = (App)App.Current;

            this.myAdapter = new OpenWeatherApiAdapter();
            this.myAdapter.GetCitiesAsyncCompleted += myAdapter_GetCitiesAsyncCompleted;
            this.myAdapter.GetCitiesAsyncFailed += myAdapter_GetCitiesAsyncFailed;
        }

        void myAdapter_GetCitiesAsyncFailed(object sender, GetCitiesAsyncFailedEventArgs e)
        {
            this.mainStackPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.myProgressLabel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.myProgressRing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.txtFail.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        void myAdapter_GetCitiesAsyncCompleted(object sender, GetCitiesAsyncCompletedEventArgs e)
        {
            if (e.Cities.Count == 0)
            {
                this.mainStackPanel.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.myProgressLabel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                this.myProgressRing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                this.txtFail.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }
            else
            {
                this.Frame.Navigate(typeof(ResultPage), e.Cities);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.mainGrid.Background = this.myApp.GetBackgroundBrush();
        }

        private void theMessage_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                sendButton_Click(sender, null);
            }
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.txtCityName.Text.Length == 0)
            {
                return;
            }

            this.mainStackPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.myProgressLabel.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.myProgressLabel.Text = this.txtCityName.Text + " wird gesucht...";
            this.myProgressRing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.myProgressRing.IsActive = true;
            this.txtFail.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            this.myAdapter.GetCitiesAsync(this.txtCityName.Text);

            return;
            /*
            IList<CityFeedEntry> c = new List<CityFeedEntry>();

            CityFeedEntry entry;
            entry = new CityFeedEntry();
            entry.City = new City();
            entry.City.Name = "Wuppertal";
            entry.City.Country = "DE";
            entry.City.Id = "12345678";
            entry.Temperature = new SimpleTemperature();
            entry.Temperature.Value = 0.15;
            c.Add(entry);

            entry = new CityFeedEntry();
            entry.City = new City();
            entry.City.Name = "Düsseldorf";
            entry.City.Country = "DE";
            entry.City.Id = "78942341";
            entry.Temperature = new SimpleTemperature();
            entry.Temperature.Value = -4.9;
            c.Add(entry);

            entry = new CityFeedEntry();
            entry.City = new City();
            entry.City.Name = "Bielefeld";
            entry.City.Country = "DE";
            entry.City.Id = "00000000";
            entry.Temperature = new SimpleTemperature();
            entry.Temperature.Value = 0.7;
            c.Add(entry);

            this.Frame.Navigate(typeof(ResultPage), c);*/
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }
    }
}
