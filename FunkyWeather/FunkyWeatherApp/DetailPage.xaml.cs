﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using OpenWeatherApi.XmlClasses;
using OpenWeatherApi.WeatherConditions;
using OpenWeatherApi.Web;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI;
using System.Globalization;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace FunkyWeatherApp
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class DetailPage : Page
    {
        private App myApp;
        private DetailedWeather myWeather;
        private OpenWeatherApiAdapter myAdapter;

        public DetailPage()
        {
            this.InitializeComponent();

            this.myApp = (App)App.Current;

            this.myAdapter = new OpenWeatherApiAdapter();
            this.myAdapter.GetWeatherDataAsyncCompleted += myAdapter_GetWeatherDataAsyncCompleted;
            this.myAdapter.GetWeatherDataAsyncFailed += myAdapter_GetWeatherDataAsyncFailed;
        }

        void myAdapter_GetWeatherDataAsyncFailed(object sender, GetWeatherDataAsyncFailedEventArgs e)
        {
            TextBlock label = new TextBlock();
            label.FontSize = 18.0D;
            label.Text = "Die Wetterdaten konnten nicht abgerufen werden.";
            spMain.Width = 700D;
            spMain.Children.Add(label);
        }

        void myAdapter_GetWeatherDataAsyncCompleted(object sender, GetWeatherDataAsyncCompletedEventArgs e)
        {
            //Wartezeug ausblenden:
            this.myProgressRing.IsActive = false;
            this.myProgressRing.Visibility = Visibility.Collapsed;

            this.myWeather = e.WeatherData;

            IList<Forecast> newForecast = new List<Forecast>(5);

            //Wir wollen nur die ersten 5 tage:
            if (this.myWeather.Forecast.Length >= 5)
	        {
                for (int i = 0; i < 5; i++)
			    {
                    newForecast.Add(this.myWeather.Forecast[i]);
			    }
	        }

            this.headline.Text = "Wettervorhersage für " + this.myWeather.Location.Name + ", " + this.myWeather.Location.Country + ":";

            //Graphen und so zeichnen:
            DrawGraphAndWeather(newForecast);
        }
        
        /// <summary>
        /// Zeichnet den Graphen und die dazugehörigen Controls mit Wetterdaten.
        /// </summary>
        /// <param name="Forecast">Die Wettervorhersage, die visualisiert werden soll.</param>
        private void DrawGraphAndWeather(IList<Forecast> Forecast)
        {
            //Koordinatensystem wird gezeichnet:
            FunkyGraphs.FunkyCoordinateSystem cs = new FunkyGraphs.FunkyCoordinateSystem(new Point(0, 0), 600, 400);
            this.mainGrid.Children.Add(cs);

            //Koordinatensystem wird konfiguriert;
            cs.Width = this.mainGrid.ActualWidth;
            cs.Height = this.mainGrid.ActualHeight;
            cs.HorizontalAlignment = HorizontalAlignment.Left;
            cs.VerticalAlignment = VerticalAlignment.Bottom;
            cs.Margin = new Thickness(0, 0, 0, 0);

            //Der FunkyGraph wird mit den aktuellen Wetterdaten gezeichnet:
            FunkyGraphs.FunkyGraph g = new FunkyGraphs.FunkyGraph(Forecast, Colors.Black, this.mainGrid.ActualWidth, this.mainGrid.ActualHeight * 0.4D);
            cs.AddFunkyGraph(g);

            //Abstände für die Wetter-Controls definieren::
            double leftOffset = 0;
            double topOffset = 300;
            double imgHeight = 120;

            for (int i = 0; i < g.Foreacst.Count; i++)
            {
                Forecast fore = g.Foreacst[i];

                double oLeft, oTop;

                //Bild laden und formatieren:
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.UriSource = new System.Uri("ms-appx:///Resources/Images/Icons/" + g.Foreacst[i].Symbol.FileName + ".png");
                Image img = new Image();
                img.Margin = new Thickness(0.0);
                img.MaxHeight = imgHeight;
                img.MaxWidth = imgHeight;
                img.Source = bitmapImage;
                img.Stretch = Stretch.Uniform;
                oLeft = g.Coordinates[i].X - leftOffset - (img.MaxWidth / 2);
                oTop = (cs.Height - g.Height) + g.Coordinates[i].Y - topOffset;
                Canvas.SetLeft(img, oLeft);
                Canvas.SetTop(img, oTop);
                cs.Children.Add(img);

                //Label mit Wochentag hinzufügen:
                TextBlock text1 = new TextBlock();
                text1.FontSize = 24.0D;
                text1.Text += fore.Day.ToString("dddd", new CultureInfo("de-DE")) + Environment.NewLine; //Gibt den Wochentag in deutsch aus: "Montag" etc.
                oTop += (imgHeight + 10);
                Canvas.SetLeft(text1, oLeft);
                Canvas.SetTop(text1, oTop);
                cs.Children.Add(text1);

                //Label mit Wetterdaten hinzufügen:
                TextBlock text2 = new TextBlock();
                text2.FontSize = 18.0D;
                text2.Text += fore.Temperature.Day + "°C" + Environment.NewLine;
                text2.Text += this.myApp.WeatherConditionCodes.Conditions[fore.Symbol.Number] + Environment.NewLine;
                oTop += (30);
                Canvas.SetLeft(text2, oLeft);
                Canvas.SetTop(text2, oTop);
                cs.Children.Add(text2);
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.headline.Text = "Wettervorhersage wird geladen...";
            this.myProgressRing.IsActive = true;
            this.myProgressRing.Visibility = Visibility.Visible;
            this.mainGrid.Background = this.myApp.GetBackgroundBrush();
            this.myAdapter.Get7DayForecastByIdAsync(this.myApp.CityId);
        }
    }
}
