﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using OpenWeatherApi.Web;
using OpenWeatherApi.XmlClasses;
using OpenWeatherApi.WeatherConditions;
using Windows.UI.Notifications;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace FunkyWeatherApp
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private OpenWeatherApiAdapter myAdapter;
        private CityFeedEntry myCity;
        private App myApp;
        private bool weatherFailed;

        public MainPage()
        {
            this.InitializeComponent();

            this.myApp = (App)App.Current;

            this.myAdapter = new OpenWeatherApiAdapter();
            this.myAdapter.GetCurrentAsyncCompleted += myAdapter_GetCurrentAsyncCompleted;
            this.myAdapter.GetCurrentAsyncFailed += myAdapter_GetCurrentAsyncFailed;
        }

        #region Show/Hide
        private void ShowCityData()
        {
            this.btnCity.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.lblCityName.Visibility = Visibility.Visible;
            this.lblCityWeather.Visibility = Visibility.Visible;
            this.imgCityWeather.Visibility = Visibility.Visible;
        }

        private void HideCityData()
        {
            this.btnCity.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.lblCityName.Visibility = Visibility.Collapsed;
            this.lblCityWeather.Visibility = Visibility.Collapsed;
            this.imgCityWeather.Visibility = Visibility.Collapsed;
        }

        private void ShowCityLoading()
        {
            this.cityProgressRing.IsActive = true;
            this.cityProgressRing.Visibility = Visibility.Visible;
        }

        private void HideCityLoading()
        {
            this.cityProgressRing.IsActive = false;
            this.cityProgressRing.Visibility = Visibility.Collapsed;
        }
        #endregion

        #region LiveTile
        private void ClearTile()
        {
            TileUpdater updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.EnableNotificationQueue(true);
            updater.Clear();
        }

        private void UpdateTile()
        {
            TileUpdater updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.EnableNotificationQueue(true);
            updater.Clear();

            XmlDocument doc = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150SmallImageAndText02);
            TileNotification notification = new TileNotification(doc);

            XmlNodeList nodes = doc.GetElementsByTagName("text");
            nodes[0].InnerText = myCity.City.Name;

            if (this.myApp.WeatherConditionCodes.Conditions.ContainsKey(myCity.Weather.Number))
            {
                nodes[1].InnerText = this.myApp.WeatherConditionCodes.Conditions[myCity.Weather.Number];
            }
            else
            {
                nodes[1].InnerText = myCity.Weather.Value;
            }

            nodes[2].InnerText = myCity.Temperature.Value.ToString("##0.0") + "°C";

            nodes = doc.GetElementsByTagName("image");
            nodes[0].Attributes.GetNamedItem("src").InnerText = "Resources/Images/Icons/" + this.myCity.Weather.Icon + ".png";

            TileNotification tile = new TileNotification(doc);

            TileUpdateManager.CreateTileUpdaterForApplication().Update(tile);
        }

        private async void RegisterBackgroundTask()
        {
            string taskName = "WeatherBackgroundTask";
            string taskEntryPoint = "BackgroundTasks.WeatherBackgroundTask";

            BackgroundAccessStatus backgroundAccessStatus = await BackgroundExecutionManager.RequestAccessAsync();

            if (backgroundAccessStatus == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity ||
                backgroundAccessStatus == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity)
            {
                foreach (var task in BackgroundTaskRegistration.AllTasks)
                {
                    if (task.Value.Name == taskName)
                    {
                        task.Value.Unregister(true);
                    }
                }

                BackgroundTaskBuilder taskBuilder = new BackgroundTaskBuilder();
                taskBuilder.Name = taskName;
                taskBuilder.TaskEntryPoint = taskEntryPoint;
                taskBuilder.SetTrigger(new TimeTrigger(15, false));
                BackgroundTaskRegistration registration = taskBuilder.Register();

                return;
            }
        }
        #endregion

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.mainGrid.Background = this.myApp.GetBackgroundBrush();

            if (!this.myApp.IsInternet())
            {
                this.headline.Visibility = Visibility.Collapsed;
                this.btnSearch.Visibility = Visibility.Collapsed;
                HideCityData();
                HideCityLoading();
                ClearTile();
                return;
            }

            if (this.myApp.CityId.Length > 0)
            {
                this.RegisterBackgroundTask();
            }

            HideCityData();
            HideCityLoading();
            this.btnCity.Visibility = Visibility.Collapsed;

            if (this.myApp.IsInternet())
            {
                if (this.myApp.CityId.Length > 0)
                {
                    HideCityData();
                    ShowCityLoading();
                    this.myAdapter.GetCurrentByIdAsync(this.myApp.CityId);
                    this.txtSearchHeadline.Text = "Umgezogen?";
                    this.txtSearchHeadline.FontSize = 24.0D;
                    this.txtSearchSubline.Text = "Klicke hier, um eine neue Stadt auszuwählen!";
                    this.txtSearchSubline.FontSize = 18.0D;
                }
            }
        }

        void myAdapter_GetCurrentAsyncFailed(object sender, GetCurrentAsyncFailedEventArgs e)
        {
            ShowCityData();
            HideCityLoading();
            this.imgCityWeather.Visibility = Visibility.Collapsed;
            this.lblCityWeather.Visibility = Visibility.Collapsed;
            this.lblCityName.Text = "Die Wetterdaten konnten nicht abgerufen werden.\r\nHier klicken, um es erneut zu versuchen.";
            this.weatherFailed = true;
        }

        void myAdapter_GetCurrentAsyncCompleted(object sender, GetCurrentAsyncCompletedEventArgs e)
        {
            this.myCity = e.CitiesItem;

            ShowCityData();
            HideCityLoading();

            this.lblCityName.Text = this.myCity.City.Name + ", " + this.myCity.City.Country;

            this.lblCityWeather.Text = this.myCity.Temperature.Value.ToString("##0.0") + "°C | " +
                this.myApp.WeatherConditionCodes.Conditions[this.myCity.Weather.Number];

            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.UriSource = new System.Uri("ms-appx:///Resources/Images/Icons/" + this.myCity.Weather.Icon + ".png");

            this.imgCityWeather.Source = bitmapImage;

            Task.Run(() => { UpdateTile(); });
            this.weatherFailed = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SearchPage));
        }

        private void btnCity_Click(object sender, RoutedEventArgs e)
        {
            if (this.weatherFailed)
            {
                HideCityData();
                ShowCityLoading();
                this.myAdapter.GetCurrentByIdAsync(this.myApp.CityId);
            }
            else
            {
                this.Frame.Navigate(typeof(DetailPage));
            }
        }
    }
}
