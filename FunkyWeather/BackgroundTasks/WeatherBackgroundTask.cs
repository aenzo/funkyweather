﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.Data.Xml.Dom;
using Windows.Storage;
using OpenWeatherApi.XmlClasses;
using OpenWeatherApi.WeatherConditions;
using OpenWeatherApi.Web;

namespace BackgroundTasks
{
    public sealed class WeatherBackgroundTask : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            CityFeedEntry c = null;
            try
            {
                c = UpdateWeather();
            }
            catch (Exception)
            {
                TileUpdater updater = TileUpdateManager.CreateTileUpdaterForApplication();
                updater.Clear();
                return;
            }

            if (c == null)
            {
                return;
            }

            UpdateTile(c);

            deferral.Complete();
        }

        private CityFeedEntry UpdateWeather()
        {
            OpenWeatherApiAdapter a = new OpenWeatherApiAdapter();

            string cityId = String.Empty;

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("CityId"))
            {
                cityId = (String)ApplicationData.Current.LocalSettings.Values["CityId"]; 
            }

            if (cityId.Length == 0)
            {
                return null;
            }

            CityFeedEntry c = a.GetCurrentByID(cityId);

            return c;           
        }

        private void UpdateTile(CityFeedEntry City)
        {
            WeatherConditionCodes w = new WeatherConditionCodes();

            TileUpdater updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.EnableNotificationQueue(true);
            updater.Clear();
            XmlDocument doc = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150SmallImageAndText02);
            TileNotification notification = new TileNotification(doc);

            XmlNodeList nodes = doc.GetElementsByTagName("text");
            nodes[0].InnerText = City.City.Name;

            if (w.Conditions.ContainsKey(City.Weather.Number))
            {
                nodes[1].InnerText = w.Conditions[City.Weather.Number];
            }
            else
            {
                nodes[1].InnerText = City.Weather.Value;
            }

            nodes[2].InnerText = City.Temperature.Value.ToString("##0.0") + "°C";

            nodes = doc.GetElementsByTagName("image");
            nodes[0].Attributes.GetNamedItem("src").InnerText = "Resources/Images/Icons/" + City.Weather.Icon + ".png";

            updater.Update(notification);
        }
    }
}
