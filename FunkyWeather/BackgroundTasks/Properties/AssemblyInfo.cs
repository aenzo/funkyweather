﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// Allgemeine Informationen über eine Assembly werden über die folgenden 
// Attribute gesteuert. Diese Attributwerte ändern, um die Informationen zu ändern,
// die einer Assembly zugeordnet sind.
[assembly: AssemblyTitle("BackgroundTasks")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vincenzo Penna")]
[assembly: AssemblyProduct("BackgroundTasks")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Versionsinformationen für eine Assembly enthalten die folgenden vier Werte:
//
//      Hauptversion
//      Nebenversion 
//      Buildnummer
//      Revision
//
// Es können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
//  mithilfe von '*' wie unten dargestellt übernommen werden:
// Assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguageAttribute("de-DE")]
