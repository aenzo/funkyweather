﻿using System;

namespace FunkyGraphs
{
    /// <summary>
    /// Enthält unterschiedliche Styles, nach denen ein <code>FunkyGraph</code> in einem <code>FunkyCoordinateSystem</code> dargestellt werden kann.
    /// </summary>
    public enum FunkyGraphStyle
    {
        /// <summary>
        /// Die Verbindungen zwischen den Punkten des <code>FunkyGraphs</code> werden als kubische Bézierkurve 3. Grades dargestellt. Die Abstände der Handles zu den Bézierpunkten entsprechen dabei der halben Schrittweite des Koordinatensystems.
        /// </summary>
        SmoothBezierStyle,

        /// <summary>
        /// Die Verbindungen zwischen den Punkten des <code>FunkyGraphs</code> werden als kubische Bézierkurve 3. Grades dargestellt. Die Abstände der Handles zu den Bézierpunkten entsprechen dabei der Schrittweite des Koordinatensystems.
        /// </summary>
        CascadingBezierStyle,

        /// <summary>
        /// Die Verbindungen zwischen den Punkten des <code>FunkyGraphs</code> werden als treppenartige Abstufungen dargestellt. Dabei wird von einem Punkt eine waagerechte Linie zur X-Koordinate des nächsten Punktes gezogen.
        /// </summary>
        StairTreadStyle,

        /// <summary>
        /// Die Verbindungen zwischen den Punkten des <code>FunkyGraphs</code> werden durch einfache Geraden dargestellt. Dabei wird ein Punkt durch eine Gerade unmittelbar mit dem nächsten Punkt Verbunden.
        /// </summary>
        DirectConnectionStyle
    }
}