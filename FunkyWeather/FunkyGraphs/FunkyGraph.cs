﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using OpenWeatherApi.XmlClasses;

namespace FunkyGraphs
{
    /// <summary>
    /// Stellt einen Graphen dar, der in einem FunkyCoordinateSystem gerendert werden kann.
    /// </summary>
    public class FunkyGraph
    {
        #region vars
        private FunkyGraphStyle myGraphStyle;
        private double myHeight;
        private bool myIsFilled;
        private double myMax;
        private double myMin;
        private Path myPath;
        private Brush myStrokeBrush;
        private Double myStrokeThickness;
        private Brush mySurfaceBrush;
        private IList<Forecast> myForecast;
        private IList<Double> myValues;
        private IList<Point> myCoordinates;
        private double myWidth;
        #region consts
        #endregion
        #endregion

        #region constructors

        /// <summary>
        /// Erstellt einen transparenten Graphen mit je einer Schrittweite Abstand links und rechts.
        /// </summary>
        /// <param name="Forecast">Die Wetterdaten, die abgebildet werden sollen</param>
        /// <param name="FillColor">Die Farbe des Graphen</param>
        /// <param name="Width">Die Breite des Graphen</param>
        /// <param name="Height">Die Höhe des Graphen</param>
        public FunkyGraph(IList<Forecast> Forecast, Color FillColor, Double Width, Double Height)
        {
            //So geht's nicht:
            if (Forecast.Count < 2) throw new ArgumentOutOfRangeException("Values", "Values.Count muss größer als 1 sein");

            this.myForecast = Forecast;
            this.myValues = new List<Double>();

            //Hier nochmal die Double-Werte extrahieren:
            foreach (Forecast f in Forecast)
            {
                this.myValues.Add(f.Temperature.Day);
            }

            this.myHeight = Height;
            this.myWidth = Width;

            this.myCoordinates = new List<Point>();

            this.mySurfaceBrush = new SolidColorBrush(FillColor);

            this.myPath = this.DrawGraph();
        }
        #endregion

        #region methods
        private Path DrawGraph()
        {
            //offset für negative werte
            double offset = 0;
            if (this.myValues.Min() < 0)
            {
                offset -= this.myValues.Min();
            }
            //stepX ist die horizontale schrittweite
            double stepX = Width / ((Values.Count - 1) + 2 /*2 steps für offset*/);
            //f ist die konstante für das offset von unten
            double f = 0.9D;
            //b ist die konstante für die höhe, von der die werte abgezogen müssen, um ihre höhe im koordinatensystem auszurechnen
            double b = Height * f;
            //k ist der faktor, mit dem die werte multipliziert werden, um ihre höhe im koordinatensystem auszurechnen
            double k = b / (this.myValues.Max() + offset);

            //neue liste mit umgerechneten werden, da das koordinatensystem technisch gesehen oben links anfängt, nicht unten links
            IList<double> vals = new List<double>();
            for (int i = 0; i < this.myValues.Count; i++)
            {
                vals.Add(b - (k * (this.myValues[i] + offset)));
            }

            //erstes und letztes element duplizieren (wegen seitenabstand - werden aber nicht zur normalen werteliste hinzugefügt...)
            vals.Insert(0, vals[0]);
            vals.Add(vals.Last());

            Path p = new Path();

            p.StrokeThickness = 0;
            p.Fill = this.mySurfaceBrush;

            //Transparent muss...
            p.Opacity = 0.2D;

            PathGeometry geo = new PathGeometry();
            geo.FillRule = FillRule.Nonzero;
            p.Data = geo;

            PathFigure fig = new PathFigure();
            fig.IsClosed = true;
            geo.Figures.Add(fig);

            Point point = new Point(0, vals[0]);
            fig.StartPoint = point;

            for (int i = 1; i < vals.Count; i++)
            {
                double x = stepX * i;
                double y = vals[i];

                point = new Point(x, y);

                //der letzte punkt soll - wie der erste - nicht zur liste hinzugefügt werden, weil er nur einen abstand darstellt und keinen wert repräsentiert:
                if (i < vals.Count - 1)
                {
                    this.myCoordinates.Add(point);
                }

                BezierSegment seg = new BezierSegment();
                seg.Point1 = new Point(x - (stepX / 2), vals[i - 1]);
                seg.Point2 = new Point(x - (stepX / 2), y);
                seg.Point3 = point;
                fig.Segments.Add(seg);
            }

            //falls gefüllt, linie nach unten und von dort nach links zeichnen
            LineSegment line = new LineSegment();
            line.Point = new Point(stepX * (vals.Count - 1), Height);
            fig.Segments.Add(line);
            line = new LineSegment();
            line.Point = new Point(0, Height);
            fig.Segments.Add(line);

            //Bazinga:
            return p;
        }
        #endregion

        #region props
        /// <summary>
        /// Gibt die Wetterdaten zurück, die der Graph repräsentiert.
        /// </summary>
        public IList<Forecast> Foreacst
        {
            get
            {
                return this.myForecast;
            }
        }

        /// <summary>
        /// Gibt das dem <code>FunkyGraph</code>-Objekt zu Grunde liegende Path-Objekt zurück.
        /// </summary>
        public Path Path
        {
            get
            {
                return this.myPath;
            }
        }

        /// <summary>
        /// Gibt eine Liste der Werte zurück, die der Graph repräsentiert.
        /// </summary>
        public IList<Double> Values
        {
            get
            {
                return this.myValues;
            }
        }

        /// <summary>
        /// Gibt eine Liste der Koordinaten zurück, die der Graph durchläuft.
        /// </summary>
        public IList<Point> Coordinates
        {
            get
            {
                return this.myCoordinates;
            }
        }

        /// <summary>
        /// Ruft die Breite des Graphen ab.
        /// </summary>
        public Double Width
        {
            get
            {
                return this.myWidth;
            }
        }

        /// <summary>
        /// Ruft die Höhe des höchsten Punktes des Graphen ab.
        /// </summary>
        public Double Height
        {
            get
            {
                return this.myHeight;
            }
        }

        /// <summary>
        /// Gibt an, ob die Pfadfläche gefüllt ist.
        /// </summary>
        public Boolean IsFilled
        {
            get
            {
                return this.myIsFilled;
            }
        }

        /// <summary>
        /// Gibt den angestrebten Minimalwert der Graphenmenge an.
        /// </summary>
        public Double MinValue
        {
            get
            {
                return this.myMin;
            }
        }

        /// <summary>
        /// Gibt den angestrebten Maximalwert der Graphenmenge an.
        /// </summary>
        public Double MaxValue
        {
            get
            {
                return this.myMax;
            }
        }

        /// <summary>
        /// Gibt den Brush für die Linie zurück.
        /// </summary>
        public Brush LineBrush
        {
            get
            {
                return this.myStrokeBrush;
            }
        }

        /// <summary>
        /// Gibt den Brush für die Fläche  zurück.
        /// </summary>
        public Brush SurfaceBrush
        {
            get
            {
                return this.mySurfaceBrush;
            }
        }

        #endregion
    }
}
