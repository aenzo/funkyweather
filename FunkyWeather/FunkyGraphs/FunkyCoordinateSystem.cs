﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace FunkyGraphs
{
    /// <summary>
    /// Stellt ein kartesisches Koordinatensystem dar, welches über nicht beschriftete Achsen verfügt und vereinfachte Methoden zum Hinzufügen von Graphen zur Verfügung stellt.
    /// </summary>
    public class FunkyCoordinateSystem : Canvas
    {
        #region vars
        private Brush myAxisBrush;
        private IList<FunkyGraph> myGraphs;
        private Point myStartPoint;
        #region consts
        private const double myAxisStrokeThickness = 2.0D;
        #endregion
        #endregion

        #region constructors
        /// <summary>
        /// Erstellt eine neue Instanz der FunkyCoordinateSystem-Klasse ohne Achsen.
        /// </summary>
        /// <param name="StartPoint">Legt den Abstand von der oberen linken Ecke des Koordinatensystems zu der des übergeordneten Steuerelements fest</param>
        /// <param name="InitialWidth">Legt die Breite fest, die das Koordinatensystem zu beginn haben soll</param>
        /// <param name="InitialHeight">Legt die Höhe fest, die das Koordinatensystem zu beginn haben soll</param>
        public FunkyCoordinateSystem(Point StartPoint, Double InitialWidth, Double InitialHeight)
            : this(StartPoint, InitialWidth, InitialHeight, Colors.Transparent) { }

        /// <summary>
        /// Erstellt eine neue Instanz der FunkyCoordinateSystem-Klasse mit Achsen.
        /// </summary>
        /// <param name="StartPoint">Legt den Abstand von der oberen linken Ecke des Koordinatensystems zu der des übergeordneten Steuerelements fest</param>
        /// <param name="InitialWidth">Legt die Breite fest, die das Koordinatensystem zu beginn haben soll</param>
        /// <param name="InitialHeight">Legt die Höhe fest, die das Koordinatensystem zu beginn haben soll</param>
        /// <param name="AxisColor">Die Farbe, die die Achsen haben sollen</param>
        public FunkyCoordinateSystem(Point StartPoint, Double InitialWidth, Double InitialHeight, Color AxisColor) 
            : base()
        {
            #region size def etc.
            this.myStartPoint = StartPoint;
            this.MinHeight = InitialHeight;
            this.MinWidth = InitialWidth;
            this.Height = InitialHeight;
            this.Width = InitialWidth;
            #endregion

            this.myGraphs = new List<FunkyGraph>();

            //achsen zeichnen
            this.myAxisBrush = new SolidColorBrush(AxisColor);
            this.DrawAxis();
        }
        #endregion

        #region props
        public IList<FunkyGraph> Graphs
        {
            get
            {
                return this.myGraphs;
            }
        }
        #endregion

        #region methods
        #region private
        /// <summary>
        /// Zeichnet die beiden Achsen basierend auf dem entsprechenden Brush, dem Startpunkt des Koordinatensystems sowie dessen Größe.
        /// </summary>
        private void DrawAxis()
        {
            //zuerst die ordinate
            Line line = new Line();
            line.X1 = this.myStartPoint.X;
            line.X2 = this.myStartPoint.X;
            line.Y1 = this.myStartPoint.Y;
            line.Y2 = this.myStartPoint.Y + this.Height;
            line.StrokeStartLineCap = PenLineCap.Round;
            line.StrokeEndLineCap = PenLineCap.Round;
            line.StrokeThickness = myAxisStrokeThickness;
            line.Stroke = this.myAxisBrush;
            Canvas.SetZIndex(line, 1337);
            this.Children.Add(line);

            //dann die abszisse
            line = new Line();
            line.X1 = this.myStartPoint.X;
            line.X2 = this.myStartPoint.X + this.Width;
            line.Y1 = this.myStartPoint.Y + this.Height;
            line.Y2 = this.myStartPoint.Y + this.Height;
            line.StrokeStartLineCap = PenLineCap.Round;
            line.StrokeEndLineCap = PenLineCap.Round;
            line.StrokeThickness = myAxisStrokeThickness;
            line.Stroke = this.myAxisBrush;
            Canvas.SetZIndex(line, 1337);
            this.Children.Add(line);
        }
        #endregion

        #region public
        /// <summary>
        /// Fügt einen neuen <code>FunkyGraph</code> zum Koordinatensystem zu und zeichnet es auf der Zeichenfläche.
        /// </summary>
        /// <param name="Graph"></param>
        public void AddFunkyGraph(FunkyGraph Graph)
        {
            if (Graph == null)
                throw new ArgumentOutOfRangeException("Graph", "Argument darf nicht null sein");

            this.myGraphs.Add(Graph);
            this.Children.Add(Graph.Path);

            if (Graph.IsFilled)
            {
                Graph.Path.Opacity = 1.0D;
            }

            Canvas.SetTop(Graph.Path, this.Height - Graph.Height);

            Canvas.SetZIndex(Graph.Path, this.myGraphs.Count > 0 ?
                Canvas.GetZIndex(this.myGraphs.Last<FunkyGraph>().Path)
                : 1);
        }
        #endregion

        #region overrides
        #endregion
        #endregion

    }
}
