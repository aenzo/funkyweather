﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using OpenWeatherApi.XmlClasses;
using Windows.Storage;

namespace OpenWeatherApiTest
{
    [TestClass]
    public class SerializationTest
    {
        [TestMethod]
        public void TestCitiesSerialization()
        {
            CityFeed foo = CityFeed.FromXmlString(GetBostonStringAsync().Result);
            Assert.IsTrue(foo.Items.Count > 0);
            if (foo.Items.Count > 0)
            {
                Assert.IsTrue(foo.Items[0].City.Name.ToLower() == "boston");
            }
        }

        private async Task<String> GetBostonStringAsync()
        {
            StorageFolder appFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            StorageFolder conditionsFolder = await appFolder.GetFolderAsync("Xml");
            StorageFile conditionsFile = await conditionsFolder.GetFileAsync("boston.xml");

            var r = await conditionsFile.OpenReadAsync();
            Stream s = r.AsStreamForRead();

            string xml = String.Empty;

            using (StreamReader reader = new StreamReader(s))
            {
                xml = reader.ReadToEnd();
            }

            return xml;
        }
    }
}
