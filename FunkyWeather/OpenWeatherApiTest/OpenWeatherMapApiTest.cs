﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using OpenWeatherApi.Web;
using OpenWeatherApi.XmlClasses;
using Windows.Storage;

namespace OpenWeatherApiTest
{
    [TestClass]
    public class OpenWeatherMapApiTest
    {
        private const string myWuppertalId = "2805753";
        private const string myWuppertalName = "wuppertal";
        [TestMethod]
        public void TestGetCurrentByName()
        {
            OpenWeatherApiAdapter a = new OpenWeatherApiAdapter();
            CurrentCity wtal = a.GetCurrentByName(myWuppertalName);
            Assert.IsNotNull(wtal);
            Assert.AreEqual(myWuppertalName, wtal.City.Name, true);
        }

        [TestMethod]
        public void TestGetCurrentById()
        {
            OpenWeatherApiAdapter a = new OpenWeatherApiAdapter();
            CurrentCity wtal = a.GetCurrentByID(myWuppertalId);
            Assert.IsNotNull(wtal);
            Assert.AreEqual(myWuppertalId, wtal.City.Id, true);
        }
    }
}
