﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using OpenWeatherApi.WeatherConditions;

namespace OpenWeatherApiTest
{
    [TestClass]
    public class ConditionTest
    {
        [TestMethod]
        public void TestWeatherCondition()
        {
            string foo = new WeatherConditionCodes().Conditions[211];
            Assert.AreEqual(foo, "Gewitter", false);
        }
    }
}
